import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  dia;
  mes;
  anio;
  constructor() { 
    this.dia = new Date().getDate();
    this.mes = new Date().getMonth()+1;
    this.anio = new Date().getFullYear();
  }

  ngOnInit(): void {
  }

}
